﻿using System;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using System.Web;

namespace WeatherApiSimple
{
	public class Program
	{
		public static async Task<int> Main(string[] argv)
		{
			const string defaultCity = "Voronezh";
			const string apiKey = "7b6be55ecfc023f52792505653e8e278";

			string city;
			if (argv.Length == 1)
				city = argv[0];
			else
				city = defaultCity;

			// Create a URI (Uniform Resource Identifier)
			var builder = new UriBuilder("https://api.openweathermap.org/data/2.5/weather");
			var queryParameters = HttpUtility.ParseQueryString(builder.Query);
			queryParameters["q"] = city;
			queryParameters["appid"] = apiKey;
			builder.Query = queryParameters.ToString();

			// Perform a request to get coodrinates of city
			Uri uri = builder.Uri;
			var request = new HttpRequestMessage(HttpMethod.Get, uri);
			var client = new HttpClient();
			HttpResponseMessage resultCoord = await client.SendAsync(request);

			string jsonContentC = await resultCoord.Content.ReadAsStringAsync();
			JsonDocument jsonDocument = JsonDocument.Parse(jsonContentC);
			double cityLon = jsonDocument.RootElement
				.GetProperty("coord")
				.GetProperty("lon")
				.GetDouble();

			double cityLat = jsonDocument.RootElement
				.GetProperty("coord")
				.GetProperty("lat")
				.GetDouble();

			// Create a URI (Uniform Resource Identifier)
			builder = new UriBuilder("https://api.openweathermap.org/data/2.5/onecall");
			queryParameters = HttpUtility.ParseQueryString(builder.Query);
			queryParameters["lat"] = cityLat.ToString();
			queryParameters["lon"] = cityLon.ToString();
			queryParameters["appid"] = apiKey;
			builder.Query = queryParameters.ToString();

			// Perform a request to get temperature and date
			uri = builder.Uri;
			request = new HttpRequestMessage(HttpMethod.Get, uri);
			client = new HttpClient();
			HttpResponseMessage result = await client.SendAsync(request);

			string jsonContentTD = await result.Content.ReadAsStringAsync();
			JsonDocument jsonDocumentTD = JsonDocument.Parse(jsonContentTD);
			double KelvinDegrees = jsonDocumentTD.RootElement
				.GetProperty("current")
				.GetProperty("temp")
				.GetDouble();

			int dateUnixTimeSeconds = jsonDocumentTD.RootElement
				.GetProperty("current")
				.GetProperty("dt")
				.GetInt32();

			DateTimeOffset offsetUtc = DateTimeOffset.FromUnixTimeSeconds(dateUnixTimeSeconds);
			DateTimeOffset offsetLocal = offsetUtc.ToLocalTime();

			Console.WriteLine("Temp in celsius: {0}, date: {1}", KelvinToCelsius(KelvinDegrees), offsetLocal);

			return 0;
		}

		private static double KelvinToCelsius(double kelvinDegrees)
		{
			return kelvinDegrees - 273.15;
		}
	}
}