﻿using EFCoreExample.DataAccess.Entity;
using System;

namespace EFCoreExample.Dto
{
	public class BookingDto
	{
		public string Roomname { get; set; }
		public string Username { get; set; }
		public DateTime FromUtc { get; set; }
		public DateTime ToUtc { get; set; }
		public string Comment { get; set; }

		public Booking ToBooking(int userId, int roomId)
		{
			return new Booking
			{
				UserId = userId,
				RoomId = roomId,
				FromUtc = DateTime.SpecifyKind(FromUtc, DateTimeKind.Utc),
				ToUtc = DateTime.SpecifyKind(ToUtc, DateTimeKind.Utc),
				Comment = Comment
			};
		}

		public static BookingDto FromBooking(Booking booking)
		{
			return new BookingDto
			{
				Comment = booking.Comment,
				FromUtc = booking.FromUtc,
				ToUtc = booking.ToUtc,
				Username = booking.User.UserName,
				Roomname = booking.Room.RoomName
			};
		}
	}
}
